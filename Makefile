BIN=app-poc5g

all:
	$(CC) $(CFLAGS) $(wildcard src/*.c) -o $(BIN) $(LDFLAGS)

clean:
	rm $(BIN)